Mass Mailer
===========
Extension for mass mailing

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist jluct/yii2-mass-mailer "*"
```

or add

```
"jluct/yii2-mass-mailer": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?php echo  \jluct\\mailer\\AutoloadExample::widget(); ?>```